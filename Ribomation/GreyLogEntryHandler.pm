#############################################
# Sample Apache HTTPd access log-reader
# The log-format expected below is based on
# a non-standard settings.
# Ensure you check/update the parsing rules.
# ------------------------------------------

package Ribomation::GreyLogEntryHandler;
use strict;
use warnings;
use Carp;
use parent 'Ribomation::LogEntryHandler';
use Switch;
use Data::Dumper;
######################################
# Constructor
# ------------------------------------
sub new {
    my ($target) = @_;
    my $class    = ref($target) || $target;    
    my $this     = $class->SUPER::new();    
    return $this;
}

######################################
# Handle log-line
# ------------------------------------
sub handle {
	my ($this, $logline, $repo, $searchStrings) = @_;

	foreach my $string (@{$searchStrings})
	{
		if ($logline=~/$string/i)
		{
			$repo->metric("LogReader|Found|$string:Value","IntAverage")->add(1) if defined $repo;
		}
	}

#	print "[GreyLogEntryHandler] '$logline'\n";# if $this->debug;
#        $repo->metric('Grey|Metric:Value')->add(1) if defined $repo;
#	$repo->metric("Grey|request:Get Scoring Result Method Failed:Value")->add(1) if defined $repo;
#	$repo->metric("Grey|request:HttpsOutboundChain:Value")->add(1) if defined $repo;



        ###Nov  6 10:10:59 DPUAT02 [host_services][0x80e00073][latency
	###Nov  6 10:10:59 DPUAT02 [host_services][0x80e00073][latency][info] mpgw(IMS): trans(2969634449)[22.146.144.10] gtid(2969634449): Latency:   0   5   0   5   5   4   1  25  50  25  50  50  49  26   5   5 [https://10.6.123.138:29882/services/WQlistDocumentEntriesNotConfirmedV1]
	
#	if ($logline =~ /.*\[(.*services)\]\[0x80e00073\]\[latency\]\[info\]\smpgw\((.*)\):\strans\((.*)\).*Latency:([\s\d]+)\s\[https:.*services\/(.*)\]$/) {
#		print "[ApacheAccessLog] :count INPUT '$logline'\n"  
#			if $this->debug;
#		my $domain   = $1;
#		my $service = $2;
#		my $transaction = $3;
#		my $val = $4;
#		my $call = $5;
#		my @latency = split(/\s{1,}/, $val);
#		print "[ApacheAccessLog] Domain=$domain, Service=$service, xxx=$call \n" 
#			if $this->debug;
#		$repo->metric("$domain|$service|$call: name","StringEvent","$call")->value($call) if defined $repo;
#		$repo->metric("$domain|$service|$call|request:HTTP headers parsed [ms]")->add($latency[1]) if defined $repo;
#		$repo->metric("$domain|$service|$call|request:begin processing [ms]")->add($latency[2]) if defined $repo;
#		$repo->metric("$domain|$service|$call|request:data completely parsed [ms]")->add($latency[3]) if defined $repo;
#		$repo->metric("$domain|$service|$call|request:XSLT parsed and ready [ms]")->add($latency[4]) if defined $repo;
#		$repo->metric("$domain|$service|$call|request:schema validated XSLT execution complete [ms]")->add($latency[5]) if defined $repo;
#		$repo->metric("$domain|$service|$call|back:attempting connection [ms]")->add($latency[6]) if defined $repo;
#		$repo->metric("$domain|$service|$call|back:connection established [ms]")->add($latency[7]) if defined $repo;
#		$repo->metric("$domain|$service|$call|back:HTTP headers sent [ms]")->add($latency[8]) if defined $repo;
#		$repo->metric("$domain|$service|$call|back:request completely transmitted [ms]")->add($latency[9]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:HTTP headers received from backside [ms]")->add($latency[10]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:begin processing [ms]")->add($latency[11]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:data from backside completely parsed [ms]")->add($latency[12]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:XSLT  parsed and ready [ms]")->add($latency[13]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:schema validated XSLT execution complete [ms]")->add($latency[14]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:HTTP headers transmitted to client [ms]")->add($latency[15]) if defined $repo;
#		$repo->metric("$domain|$service|$call|response:response completely transmitted [ms]")->add($latency[16]) if defined $repo;
#		$repo->metric("$domain|$service|$call|Transaction:count","IntRate")->add('1') if defined $repo;
#		$repo->metric("$domain|$service|$call|Transaction:count")->ascount('1') if defined $repo;
#		$repo->metric("$domain|$service|$call|Transaction:count2")->{count} if defined $repo;
#		$repo->metric("$domain|$service|$call|Transacion:id")->add($transaction) if defined $repo;
#	}




}

sub code2text {
	my ($code) = @_;
	switch ($code) {
		case 200 {return 'OK'}
		case 201 {return 'Created'}
		case 400 {return 'Bad Request'}
		case 401 {return 'Unauthorized'}
		case 403 {return 'Forbidden'}
		case 404 {return 'Not Found'}
		case 405 {return 'Method Not Allowed'}
		case 406 {return 'Not Acceptable'}
		case 418 {return 'I am a teapot (RFC 2324)'}
		case 500 {return 'Server Error'}
		case 501 {return 'Not Implemented'}
		case 503 {return 'Unavailable'}
	}
	return $code;
}

1;
